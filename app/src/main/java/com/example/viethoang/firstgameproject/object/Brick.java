package com.example.viethoang.firstgameproject.object;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import com.example.viethoang.firstgameproject.helper.AssetLoader;

/**
 * Created by VietHoang on 16/01/2016.
 */
public class Brick extends GameObject {

    public Brick(float x, float y, float width, float height) {
        super();
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        bitmap = Bitmap.createScaledBitmap(AssetLoader.brick, (int) getWidth(), (int) getHeight(), false);
    }

    public void draw(Canvas canvas){
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.WHITE);
        canvas.drawRect(getX(), getY(), getX() + getWidth(), getY() + getHeight(), paint);
    }
}
